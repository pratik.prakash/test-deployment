const express = require("express");
const app = express();
let port = 8080;

app.get("/api/test", (req, res, next) => {
  console.log("test is running");
  return res.send("Server is running");
});
app.get("/", (req, res, next) => {
  console.log("test is running");
  return res.send("Main rout is running on 8080");
});
app.listen(port, () => {
  console.log(`server running on port ${port}`);
});
